import { createApp } from 'vue';
import './style.css';
import App from './App.vue';
import router from './router';

import AOS from 'aos';
import 'aos/dist/aos.css';

import PrimeVue from 'primevue/config';
import 'primevue/resources/themes/aura-light-green/theme.css'


import PureCounter from "@srexi/purecounterjs";
const pure = new PureCounter();

const app = createApp(App);

// Monta la aplicación Vue
app.use(router);
app.use(PrimeVue);
app.mount('#app');

// Inicializa AOS después de que la aplicación Vue se ha montado
AOS.init();