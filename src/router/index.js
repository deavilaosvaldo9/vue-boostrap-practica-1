import { createRouter, createWebHistory } from 'vue-router';

import Inicio from '../components/views/Inicio.vue';
import QuienesSomos from '../components/views/QuienesSomos.vue';
import Contactenos from '../components/views/Contactenos.vue';

const routes = [
  {
    path: '/',
    name: 'Inicio',
    component: Inicio
  },
  {
    path: '/quienes-somos',
    name: 'QuienesSomos',
    component: QuienesSomos
  },
  {
    path: '/contactenos',
    name: 'Contactenos',
    component: Contactenos
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
